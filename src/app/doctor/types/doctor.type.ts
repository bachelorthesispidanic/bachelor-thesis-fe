export interface PatientListResponseDto {
  id: string;
  firstName: string;
  lastName: string;
  identificationNumber: string;
}

export interface NewPatientDto {
  firstName: string;
  lastName: string;
  identificationNumber: string;
  birthDate: Date;
  email: string;
  phone: string;
  street: string;
  houseNumber: string;
  zipCode: string;
  city: string;
  country: string;
  insuranceCode: number;
  weight: number;
  height: number;
  sexId: string;
  gpId: string;
}

export interface SexDto {
  abbreviation: string;
  code: string;
  id: string;
  name: string;
  nameEn: string;
}
