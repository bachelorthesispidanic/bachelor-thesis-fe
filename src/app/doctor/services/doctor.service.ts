import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PaginationResponseDto } from 'src/app/common/types/common.type';
import { environment } from 'src/environments/environment';
import { PatientListResponseDto } from '../types/doctor.type';

@Injectable({
  providedIn: 'root',
})
export class DoctorService {
  constructor(private readonly http: HttpClient) {}

  getDoctorsPatients(page?: number, pageSize?: number, search?: string) {
    return this.http.get<PaginationResponseDto<PatientListResponseDto[]>>(
      environment.baseUrl + 'patient/doctor',
      {
        params: {
          page: page ?? 1,
          limit: pageSize ?? 10,
          search: search ?? '',
        },
      }
    );
  }

  getSexes() {
    return this.http.get<any[]>(environment.baseUrl + 'sex');
  }
}
