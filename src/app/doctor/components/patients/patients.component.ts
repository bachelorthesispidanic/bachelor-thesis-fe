import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import {
  catchError,
  combineLatest,
  concatMap,
  filter,
  map,
  of,
  startWith,
  Subject,
  take,
  takeUntil,
  tap,
  throwError,
} from 'rxjs';
import { DisposableComponent } from 'src/app/common/components/disposable/disposable.component';
import { NotificationService } from 'src/app/common/services/notification.service';
import { DoctorService } from '../../services/doctor.service';
import { PatientListResponseDto } from '../../types/doctor.type';
import { NewPatientDialogComponent } from '../new-patient-dialog/new-patient-dialog.component';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss'],
})
export class PatientsComponent
  extends DisposableComponent
  implements AfterViewInit
{
  patientsDataSource: MatTableDataSource<PatientListResponseDto>;
  displayedColumns: string[] = [
    'identificationNumber',
    'firstName',
    'lastName',
    'birthDate',
  ];
  resultsLength = 0;
  tableData: PatientListResponseDto[] = [];
  searchForm: FormGroup;
  @ViewChild('paginator') paginator: MatPaginator;
  fetchPatients$: Subject<void> = new Subject();

  constructor(
    private readonly doctorService: DoctorService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private readonly notificationService: NotificationService
  ) {
    super();
    this.searchForm = new FormGroup({
      searchInput: new FormControl('', Validators.maxLength(100)),
    });
  }

  ngAfterViewInit(): void {
    const fetchObs$ = this.fetchPatients$.asObservable().pipe(
      takeUntil(this.destroySignal$),
      map(() => null)
    );

    const qpObs$ = this.activatedRoute.queryParams.pipe(
      takeUntil(this.destroySignal$),
      tap((qp) => {
        this.paginator.pageSize = Number(qp.pageSize) ?? 10;
        this.paginator.pageIndex = Number(qp.page) - 1 ?? 0;
      }),
      map((qp) => qp)
    );

    combineLatest([fetchObs$, qpObs$])
      .pipe(
        takeUntil(this.destroySignal$),
        map(([, qp]) => qp),
        startWith({ page: 1, pageSize: 10, search: '' }),
        concatMap((qp) =>
          this.doctorService.getDoctorsPatients(qp.page, qp.pageSize, qp.search)
        ),
        map((res) => {
          this.tableData = res.items;
          this.resultsLength = res.meta.totalItems;
          this.patientsDataSource = new MatTableDataSource(this.tableData);
        })
      )
      .subscribe();
  }

  addPatient() {
    const dialogRef = this.dialog.open(NewPatientDialogComponent, {
      width: '500px',
      maxWidth: '95%',
      data: {
        title: 'Nový záznam',
      },
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter((res) => res != null),
        concatMap((res) => {
          console.log(res);
          return of(true);
        }),
        // concatMap((res: string) => {
        //   return this.patientManagementService.createPersonalAnamnesis({
        //     patientId: this.patientId,
        //     description: res,
        //   });
        // }),
        tap(() => {
          this.notificationService.success('Záznam bol úspešne vytvorený.');
          this.fetchPatients$.next();
        }),
        catchError((err) => {
          this.notificationService.error('Záznam sa nepodarilo vytvoriť.');
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }

  searchPatient() {
    if (this.searchForm.value.searchInput !== '') {
      this.router.navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: { search: this.searchForm.value.searchInput },
      });
    }
  }

  handlePagination() {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        page: this.paginator.pageIndex + 1,
        pageSize: this.paginator.pageSize,
      },
      queryParamsHandling: 'merge',
    });
  }
}
