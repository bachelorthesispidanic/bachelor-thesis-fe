import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { map, take } from 'rxjs';
import { DisposableComponent } from 'src/app/common/components/disposable/disposable.component';
import { DoctorService } from '../../services/doctor.service';
import { SexDto } from '../../types/doctor.type';

@Component({
  selector: 'app-new-patient-dialog',
  templateUrl: './new-patient-dialog.component.html',
  styleUrls: ['./new-patient-dialog.component.scss'],
})
export class NewPatientDialogComponent
  extends DisposableComponent
  implements OnInit
{
  patientForm: FormGroup;
  sexOptions: SexDto[];
  sexControl = new FormControl(null);
  isLoaded = false;

  constructor(
    public dialogRef: MatDialogRef<NewPatientDialogComponent>,
    private readonly doctorService: DoctorService
  ) {
    super();
    dialogRef.disableClose = true;
    this.patientForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      identificationNumber: new FormControl('', Validators.required),
      birthDate: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      houseNumber: new FormControl('', Validators.required),
      zipCode: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required),
      insuranceCode: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      height: new FormControl('', Validators.required),
      gpId: new FormControl('', Validators.required),
      sexId: this.sexControl,
    });
  }

  ngOnInit(): void {
    this.doctorService
      .getSexes()
      .pipe(
        take(1),
        map((res) => (this.sexOptions = res))
      )
      .subscribe(() => (this.isLoaded = true));
  }

  onClick(result: any) {
    this.dialogRef.close(result);
  }

  onSubmit() {
    // this.onClick(this.patientForm.value);
    console.log(this.patientForm.value);
    console.log(this.patientForm.valid);
  }
}
