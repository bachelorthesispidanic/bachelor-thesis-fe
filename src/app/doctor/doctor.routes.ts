import { Route } from '@angular/router';
import { NewAppointmentComponent } from './components/new-appointment/new-appointment.component';
import { PatientsComponent } from './components/patients/patients.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { WaitingRoomComponent } from './components/waiting-room/waiting-room.component';

export const DOCTOR_ROUTES: Route[] = [
  {
    path: '',
    component: SidenavComponent,
    children: [
      {
        path: '',
        redirectTo: 'patients',
        pathMatch: 'full',
      },
      {
        path: 'patients',
        component: PatientsComponent,
      },
      {
        path: 'waiting-room',
        component: WaitingRoomComponent,
      },
      {
        path: 'new-appointment',
        component: NewAppointmentComponent,
      },
    ],
  },
];
