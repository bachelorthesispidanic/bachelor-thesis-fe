import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { PatientsComponent } from './components/patients/patients.component';
import { WaitingRoomComponent } from './components/waiting-room/waiting-room.component';
import { NewAppointmentComponent } from './components/new-appointment/new-appointment.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { DOCTOR_ROUTES } from './doctor.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewPatientDialogComponent } from './components/new-patient-dialog/new-patient-dialog.component';

@NgModule({
  declarations: [
    SidenavComponent,
    PatientsComponent,
    WaitingRoomComponent,
    NewAppointmentComponent,
    NewPatientDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(DOCTOR_ROUTES),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class DoctorModule {}
