import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@ngneat/reactive-forms';
import { Store } from '@ngxs/store';
import { take, tap, catchError, throwError } from 'rxjs';
import { NotificationService } from 'src/app/common/services/notification.service';
import { AuthService } from '../../services/auth.service';
import { Login } from '../../state/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  hidePassword = true;
  loginForm: FormGroup<any>;
  returnUrl: string;

  constructor(
    private readonly authService: AuthService,
    private readonly store: Store,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
    private readonly route: ActivatedRoute
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
  }

  onSubmit() {
    const { email, password } = this.loginForm.value;
    this.authService
      .performLogin(email, password)
      .pipe(
        take(1),
        tap((res) => {
          this.store.dispatch([new Login(res)]);
          this.notificationService.success('Prihlásenie úspešné');
          this.router.navigateByUrl(this.returnUrl);
        }),
        catchError((err) => {
          console.log(err);
          const errorMessage = 'Nesprávne prihlasovacie údaje';
          this.notificationService.error(errorMessage);
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }
}
