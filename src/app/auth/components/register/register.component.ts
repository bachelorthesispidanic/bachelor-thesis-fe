import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { Store } from '@ngxs/store';
import { NotificationService } from 'src/app/common/services/notification.service';
import { emailValidator } from 'src/app/utils/forms/validators/email.validator';
import { equalityValidator } from 'src/app/utils/forms/validators/equality.validator';
import { AuthService } from '../../services/auth.service';

interface RegisterForm {
  password: string;
  confirmPassword: string;
  email: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  hidePassword = true;
  hideConfirmPassword = true;
  registerForm: FormGroup<any>;

  constructor(
    private readonly authService: AuthService,
    private readonly store: Store,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
    private readonly route: ActivatedRoute
  ) {
    this.registerForm = new FormGroup({
      email: new FormControl(
        '',
        Validators.compose([Validators.required, emailValidator])
      ),
      password: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(
            '^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$'
          ),
        ])
      ),
      confirmPassword: new FormControl(
        '',
        Validators.compose([equalityValidator('password'), Validators.required])
      ),
    });
  }

  formControlTouched(name: keyof RegisterForm) {
    return this.registerForm.controls[name].touched;
  }

  ngOnInit(): void {}

  onSubmit() {
    const { email, password, passwordConfirmation } = this.registerForm.value;
    console.log(email, password, passwordConfirmation);
    // this.authService
    //   .performLogin(email, password)
    //   .pipe(
    //     take(1),
    //     tap((res) => {
    //       this.store.dispatch([new Login(res)]);
    //       this.notificationService.success('Prihlásenie úspešné');
    //       this.router.navigateByUrl(this.returnUrl);
    //     }),
    //     catchError((err) => {
    //       console.log(err);
    //       const errorMessage = 'Nesprávne prihlasovacie údaje';
    //       this.notificationService.error(errorMessage);
    //       return throwError(() => new Error(err));
    //     })
    //   )
    //   .subscribe();
  }
}
