import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AUTH_ROUTES } from './auth.routes';
import { NgxsModule } from '@ngxs/store';
import { CommonAppModule } from '../common/common.module';
import { AuthState } from './state/auth.state';
import { MaterialModule } from '../material.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AUTH_ROUTES),
    ReactiveFormsModule,
    FormsModule,
    CommonAppModule,
    HttpClientModule,
    NgxsModule.forFeature([AuthState]),
    MaterialModule,
  ],
})
export class AuthModule {}
