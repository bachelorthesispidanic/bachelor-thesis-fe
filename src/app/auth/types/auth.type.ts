export interface LoginResponseDto {
  accessToken: string;
  refreshToken: string;
  email: string;
  userId: string;
}

export interface RefreshTokenResponseDto {
  accessToken: string;
}
