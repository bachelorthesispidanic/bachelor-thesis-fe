import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, StateToken } from '@ngxs/store';
import { Login, Logout, UpdateAuthState } from './auth.actions';
import type { AuthStateModel } from './auth.model';

const AUTH_STATE_TOKEN = new StateToken<AuthStateModel>('auth');

function createInitialState(): AuthStateModel {
  return {
    accessToken: '',
    refreshToken: '',
  };
}

@State({
  name: AUTH_STATE_TOKEN,
  defaults: createInitialState(),
})
@Injectable()
export class AuthState {
  @Selector([AuthState])
  static accessToken(state: AuthStateModel) {
    return state.accessToken;
  }

  @Selector([AuthState])
  static refreshToken(state: AuthStateModel) {
    return state.refreshToken;
  }

  @Selector([AuthState])
  static isLoggedIn(state: AuthStateModel) {
    return !!state.accessToken;
  }

  @Action(Login)
  public login(
    { patchState }: StateContext<AuthStateModel>,
    { payload }: Login
  ) {
    const { refreshToken, accessToken } = payload;
    return patchState({ refreshToken, accessToken });
  }

  @Action(UpdateAuthState)
  public updateState(
    { patchState }: StateContext<AuthStateModel>,
    { payload }: UpdateAuthState
  ) {
    return patchState(payload);
  }

  @Action(Logout)
  public async logout({ setState }: StateContext<AuthStateModel>) {
    return setState(createInitialState());
  }
}
