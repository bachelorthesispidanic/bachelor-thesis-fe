import type { AuthStateModel } from './auth.model';

class Login {
  static readonly type = '[Auth] Login';
  constructor(public payload: AuthStateModel) {}
}

class UpdateAuthState {
  static readonly type = '[Auth] Update state';
  constructor(public payload: Partial<AuthStateModel>) {}
}

class Logout {
  static readonly type = '[Auth] Logout';
}

export { Login, Logout, UpdateAuthState };
