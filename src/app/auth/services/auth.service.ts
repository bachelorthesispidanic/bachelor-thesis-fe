import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { environment } from 'src/environments/environment';
import { AuthState } from '../state/auth.state';
import { LoginResponseDto } from '../types/auth.type';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly store: Store
  ) {}

  performLogin(email: string, password: string) {
    return this.httpClient.post<LoginResponseDto>(
      environment.baseUrl + 'auth/login',
      { email, password }
    );
  }

  performLogout() {
    return this.httpClient.post(environment.baseUrl + 'auth/logout', {
      refreshToken: this.store.selectSnapshot(AuthState.refreshToken),
    });
  }

  performRegistration(registrationData: {
    [key: string]: string | boolean | number;
  }) {
    return this.httpClient.post(
      environment.baseUrl + 'auth/register',
      registrationData
    );
  }

  performTokenRefresh(refreshToken: string) {
    return this.httpClient.post(environment.baseUrl + 'auth/refresh-token', {
      refreshToken: refreshToken,
    });
  }
}
