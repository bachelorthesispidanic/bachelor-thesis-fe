import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-anamnesis-dialog',
  templateUrl: './anamnesis-dialog.component.html',
  styleUrls: ['./anamnesis-dialog.component.scss'],
})
export class AnamnesisDialogComponent implements OnInit {
  anamnesisForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<AnamnesisDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { title: string; description?: string }
  ) {
    dialogRef.disableClose = true;
    this.anamnesisForm = new FormGroup({
      description: new FormControl(
        this.data.description ?? '',
        Validators.required
      ),
    });
  }

  ngOnInit(): void {}

  get isFormValid() {
    return this.anamnesisForm.valid;
  }

  onSubmit() {
    this.onClick(this.anamnesisForm.value.description);
  }

  onClick(result: string): void {
    console.log(result);
    this.dialogRef.close(result);
  }
}
