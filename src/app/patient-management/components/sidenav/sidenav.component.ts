import { Component, HostBinding, OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { MatDrawerMode } from '@angular/material/sidenav';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import {
  Observable,
  BehaviorSubject,
  takeUntil,
  tap,
  concatMap,
  map,
  take,
} from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Logout } from 'src/app/auth/state/auth.actions';
import { DisposableComponent } from 'src/app/common/components/disposable/disposable.component';
import { UpdateAppState } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';
import { RouteStateService } from 'src/app/utils/services/route-state.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent extends DisposableComponent implements OnInit {
  sidenavOpened$: Observable<boolean> | undefined;
  sidenavMode$: Observable<MatDrawerMode>;
  userId: string;
  isOwner$ = new BehaviorSubject<boolean>(undefined);
  patient;
  isLoading$ = new BehaviorSubject<boolean>(true);

  constructor(
    private readonly authService: AuthService,
    private readonly store: Store,
    private readonly mediaObserver: MediaObserver,
    private readonly route: ActivatedRoute,
    private readonly routeStateService: RouteStateService
  ) {
    super();
  }

  ngOnInit(): void {
    // this.route.params
    //   .pipe(
    //     takeUntil(this.destroySignal$),
    //     tap((params) => (this.userId = params.id)),
    //     tap(() =>
    //       this.isOwner$.next(
    //         this.store.selectSnapshot(UserState.id) == this.userId
    //       )
    //     )
    //   )
    //   .subscribe();
    // this.isOwner$
    //   .asObservable()
    //   .pipe(
    //     takeUntil(this.destroySignal$),
    //     concatMap((value) => {
    //       if (value) {
    //         return this.store.select(UserState.profile);
    //       } else {
    //         return this.userService.getPublicProfile(this.userId);
    //       }
    //     }),
    //     tap((res) => {
    //       this.profile = res;
    //       this.isLoading$.next(false);
    //     })
    //   )
    //   .subscribe();
    this.sidenavOpened$ = this.store.select(AppState.isSidenavOpened);
    this.sidenavMode$ = this.mediaObserver.asObservable().pipe(
      takeUntil(this.destroySignal$),
      map((change) =>
        change.some((i) => ['sm', 'xs'].includes(i.mqAlias)) ? 'over' : 'side'
      )
    );

    this.route.params
      .pipe(
        map((params) => params.id),
        takeUntil(this.destroySignal$)
      )
      .subscribe((routePathParam = '') => {
        this.routeStateService.updatePathParamState({ id: routePathParam });
      });
  }

  collapseOnClick() {
    this.sidenavMode$.pipe(take(1)).subscribe((value) => {
      if (value === 'over') {
        this.store.dispatch(new UpdateAppState({ isSidenavOpened: false }));
      }
    });
  }
}
