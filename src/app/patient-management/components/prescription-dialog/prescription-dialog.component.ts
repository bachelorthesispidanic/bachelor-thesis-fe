import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { MatDialogRef } from '@angular/material/dialog';
import {
  takeUntil,
  debounceTime,
  distinctUntilChanged,
  concatMap,
  map,
  catchError,
  forkJoin,
  of,
  take,
  tap,
  throwError,
} from 'rxjs';
import { DisposableComponent } from 'src/app/common/components/disposable/disposable.component';
import { NotificationService } from 'src/app/common/services/notification.service';
import { RouteStateService } from 'src/app/utils/services/route-state.service';
import { WINDOW_REF, WindowRef } from 'src/app/utils/window-ref';
import { PatientManagementService } from '../../services/patient-management.service';
import {
  MedicineDto,
  MedicalNeedDto,
  NewPrescriptionDto,
  NewRecordDto,
} from '../../types/patient-management.type';

@Component({
  selector: 'app-prescription-dialog',
  templateUrl: './prescription-dialog.component.html',
  styleUrls: ['./prescription-dialog.component.scss'],
})
export class PrescriptionDialogComponent
  extends DisposableComponent
  implements OnInit
{
  prescriptionForm: FormGroup;
  patientId: string;

  // suggestions
  medicineSuggestions: MedicineDto[] = [];
  medicalNeedSuggestions: MedicalNeedDto[] = [];

  // selected options
  selectedMedicines: MedicineDto[] = [];
  selectedMedicalNeeds: MedicalNeedDto[] = [];

  // refs
  @ViewChild('medicineInputNative', { static: false })
  medicineInputNative: ElementRef<HTMLInputElement>;
  @ViewChild('medicineSuggestionsAuto', { static: false })
  medicineSuggestionsAuto: MatAutocomplete;
  @ViewChild('medicalNeedInputNative', { static: false })
  medicalNeedInputNative: ElementRef<HTMLInputElement>;
  @ViewChild('medicalNeedSuggestionsAuto', { static: false })
  medicalNeedSuggestionsAuto: MatAutocomplete;

  constructor(
    private readonly patientManagementService: PatientManagementService,
    private readonly notificationService: NotificationService,
    @Inject(WINDOW_REF) private readonly window: WindowRef,
    private readonly routeStateService: RouteStateService,
    public dialogRef: MatDialogRef<PrescriptionDialogComponent>
  ) {
    super();
    dialogRef.disableClose = true;
    this.prescriptionForm = new FormGroup({
      description: new FormControl('', Validators.required),
      diagnose: new FormControl(''),
      procedure: new FormControl(''),
      medicine: new FormControl(''),
      medicalNeed: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.routeStateService.pathParam
      .pipe(
        takeUntil(this.destroySignal$),
        map((value) => (this.patientId = value.id))
      )
      .subscribe();

    this.prescriptionForm
      .get('medicine')
      .valueChanges.pipe(
        takeUntil(this.destroySignal$),
        debounceTime(300),
        distinctUntilChanged(),
        concatMap((value) =>
          this.patientManagementService.searchMedicines(value)
        ),
        map((res) => (this.medicineSuggestions = res))
      )
      .subscribe();

    this.prescriptionForm
      .get('medicalNeed')
      .valueChanges.pipe(
        takeUntil(this.destroySignal$),
        debounceTime(300),
        distinctUntilChanged(),
        concatMap((value) =>
          this.patientManagementService.searchMedicalNeeds(value)
        ),
        map((res) => (this.medicalNeedSuggestions = res))
      )
      .subscribe();
  }

  onMedicineSelected(e: MatAutocompleteSelectedEvent): void {
    if (!this.selectedMedicines.find((m) => m.id === e.option.value.id)) {
      this.selectedMedicines.push(e.option.value);
    }
    this.medicineInputNative.nativeElement.value = '';
    this.prescriptionForm.get('medicine').setValue(null);
  }

  removeMedicine(medicine) {
    this.selectedMedicines = this.selectedMedicines.filter(
      (m) => m.id !== medicine.id
    );
  }

  onMedicalNeedSelected(e: MatAutocompleteSelectedEvent): void {
    if (!this.selectedMedicalNeeds.find((m) => m.id === e.option.value.id)) {
      this.selectedMedicalNeeds.push(e.option.value);
    }
    this.medicalNeedInputNative.nativeElement.value = '';
    this.prescriptionForm.get('medicalNeed').setValue(null);
  }

  removeMedicalNeed(medicalNeed) {
    this.selectedMedicalNeeds = this.selectedMedicalNeeds.filter(
      (m) => m.id !== medicalNeed.id
    );
  }

  getMedicineInfo(code: string) {
    this.patientManagementService
      .getMedicineInfo(code)
      .pipe(
        take(1),
        tap((res) => this.window.open(res.url)),
        catchError((err) => {
          this.notificationService.error('Hľadaný liek sa nenašiel');
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }

  get isFormValid() {
    return (
      this.selectedMedicalNeeds.length > 0 || this.selectedMedicines.length > 0
    );
  }

  onClick(result: any) {
    this.dialogRef.close(result);
  }

  onSubmit(): void {
    const medicines = this.selectedMedicines.map((m) => ({
      patientId: this.patientId,
      recordId: null,
      medicineId: m.id,
      medicalNeedId: null,
    }));

    const medicalNeeds = this.selectedMedicalNeeds.map((m) => ({
      patientId: this.patientId,
      recordId: null,
      medicineId: null,
      medicalNeedId: m.id,
    }));

    const prescriptions: NewPrescriptionDto[] = medicines.concat(medicalNeeds);
    this.onClick(prescriptions);
  }
}
