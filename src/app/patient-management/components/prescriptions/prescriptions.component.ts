import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import {
  Subject,
  takeUntil,
  map,
  tap,
  combineLatest,
  startWith,
  concatMap,
  take,
  filter,
  catchError,
  throwError,
  forkJoin,
} from 'rxjs';
import { DisposableComponent } from 'src/app/common/components/disposable/disposable.component';
import { NotificationService } from 'src/app/common/services/notification.service';
import { RouteStateService } from 'src/app/utils/services/route-state.service';
import { PatientManagementService } from '../../services/patient-management.service';
import {
  NewPrescriptionDto,
  PrescriptionResponseDto,
} from '../../types/patient-management.type';
import { PrescriptionDialogComponent } from '../prescription-dialog/prescription-dialog.component';

@Component({
  selector: 'app-prescriptions',
  templateUrl: './prescriptions.component.html',
  styleUrls: ['./prescriptions.component.scss'],
})
export class PrescriptionsComponent
  extends DisposableComponent
  implements OnInit, AfterViewInit
{
  patientId: string;
  resultsLength = 0;
  prescriptions: PrescriptionResponseDto[];
  @ViewChild('paginator') paginator: MatPaginator;
  fetchPrescriptions$: Subject<void> = new Subject();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly patientManagementService: PatientManagementService,
    private readonly router: Router,
    private readonly routeStateService: RouteStateService,
    public dialog: MatDialog,
    private readonly notificationService: NotificationService
  ) {
    super();
  }

  ngAfterViewInit() {
    const fetchObs$ = this.fetchPrescriptions$.asObservable().pipe(
      takeUntil(this.destroySignal$),
      map(() => null)
    );

    const qpObs$ = this.activatedRoute.queryParams.pipe(
      takeUntil(this.destroySignal$),
      tap((qp) => {
        this.paginator.pageSize = Number(qp.pageSize) ?? 10;
        this.paginator.pageIndex = Number(qp.page) - 1 ?? 0;
      }),
      map((qp) => qp)
    );

    combineLatest([fetchObs$, qpObs$])
      .pipe(
        map(([, qp]) => qp),
        startWith({ page: 1, pageSize: 10 }),
        concatMap((qp) =>
          this.patientManagementService.getPatientsPrescriptions(
            this.patientId,
            qp.page,
            qp.pageSize
          )
        ),
        map((res) => {
          this.prescriptions = res.items;
          this.resultsLength = res.meta.totalItems;
        })
      )
      .subscribe();
  }

  ngOnInit(): void {
    this.routeStateService.pathParam
      .pipe(
        takeUntil(this.destroySignal$),
        map((value) => (this.patientId = value.id))
      )
      .subscribe();
  }

  createPrescription() {
    const dialogRef = this.dialog.open(PrescriptionDialogComponent, {
      width: '500px',
      maxWidth: '95%',
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter((res) => res != null),
        concatMap((res: NewPrescriptionDto[]) => {
          const obs = res.map((p) =>
            this.patientManagementService.createPrescription(p)
          );
          return forkJoin(obs);
        }),
        tap(() => {
          this.notificationService.success('Recept bol úspešne odoslaný.');
          this.fetchPrescriptions$.next();
        }),
        catchError((err) => {
          this.notificationService.error('Recept sa nepodarilo odoslať.');
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }

  handlePagination() {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        page: this.paginator.pageIndex + 1,
        pageSize: this.paginator.pageSize,
      },
      queryParamsHandling: 'merge',
    });
  }
}
