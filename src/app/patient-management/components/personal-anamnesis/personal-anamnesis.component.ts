import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import {
  takeUntil,
  tap,
  concatMap,
  map,
  catchError,
  filter,
  take,
  throwError,
  Subject,
  combineLatestWith,
  combineLatest,
  startWith,
} from 'rxjs';
import { DisposableComponent } from 'src/app/common/components/disposable/disposable.component';
import { NotificationService } from 'src/app/common/services/notification.service';
import { RouteStateService } from 'src/app/utils/services/route-state.service';
import { PatientManagementService } from '../../services/patient-management.service';
import { AnamnesisResponseDto } from '../../types/patient-management.type';
import { AnamnesisDialogComponent } from '../anamnesis-dialog/anamnesis-dialog.component';
import { DeleteAnamnesisDialogComponent } from '../delete-anamnesis-dialog/delete-anamnesis-dialog.component';

@Component({
  selector: 'app-personal-anamnesis',
  templateUrl: './personal-anamnesis.component.html',
  styleUrls: ['./personal-anamnesis.component.scss'],
})
export class PersonalAnamnesisComponent
  extends DisposableComponent
  implements OnInit, AfterViewInit
{
  patientId: string;
  resultsLength = 0;
  records: AnamnesisResponseDto[];
  @ViewChild('paginator') paginator: MatPaginator;
  fetchAnamnesis$: Subject<void> = new Subject();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly patientManagementService: PatientManagementService,
    private readonly router: Router,
    private readonly routeStateService: RouteStateService,
    public dialog: MatDialog,
    private readonly notificationService: NotificationService
  ) {
    super();
  }

  ngAfterViewInit() {
    const fetchObs$ = this.fetchAnamnesis$.asObservable().pipe(
      takeUntil(this.destroySignal$),
      map(() => null)
    );

    const qpObs$ = this.activatedRoute.queryParams.pipe(
      takeUntil(this.destroySignal$),
      tap((qp) => {
        this.paginator.pageSize = Number(qp.pageSize) ?? 10;
        this.paginator.pageIndex = Number(qp.page) - 1 ?? 0;
      }),
      map((qp) => qp)
    );

    combineLatest([fetchObs$, qpObs$])
      .pipe(
        map(([, qp]) => qp),
        startWith({ page: 1, pageSize: 10 }),
        concatMap((qp) =>
          this.patientManagementService.getPersonalAnamnesis(
            this.patientId,
            qp.page,
            qp.pageSize
          )
        ),
        map((res) => {
          this.records = res.items;
          this.resultsLength = res.meta.totalItems;
        })
      )
      .subscribe();
  }

  ngOnInit(): void {
    this.routeStateService.pathParam
      .pipe(
        takeUntil(this.destroySignal$),
        map((value) => (this.patientId = value.id))
      )
      .subscribe();
  }

  createAnamnesis() {
    const dialogRef = this.dialog.open(AnamnesisDialogComponent, {
      width: '500px',
      maxWidth: '95%',
      data: {
        title: 'Nový záznam',
      },
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter((res) => res != null),
        concatMap((res: string) => {
          return this.patientManagementService.createPersonalAnamnesis({
            patientId: this.patientId,
            description: res,
          });
        }),
        tap(() => {
          this.notificationService.success('Záznam bol úspešne vytvorený.');
          this.fetchAnamnesis$.next();
        }),
        catchError((err) => {
          this.notificationService.error('Záznam sa nepodarilo vytvoriť.');
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }

  removeAnamnesis(id: string) {
    const dialogRef = this.dialog.open(DeleteAnamnesisDialogComponent, {
      width: '500px',
      maxWidth: '95%',
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter((res) => res != 'no'),
        concatMap(() =>
          this.patientManagementService.deletePersonalAnamnesis(id)
        ),
        tap(() => {
          this.notificationService.success('Záznam bol úspešne odstránený.');
          this.fetchAnamnesis$.next();
        }),
        catchError((err) => {
          this.notificationService.error('Záznam sa nepodarilo odstrániť.');
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }

  editAnamnesis(id: string) {
    const dialogRef = this.dialog.open(AnamnesisDialogComponent, {
      width: '500px',
      maxWidth: '95%',
      data: {
        title: 'Upraviť záznam',
        description: this.records.find((r) => r.id === id)?.description,
      },
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter((res) => res != null),
        concatMap((res: string) => {
          return this.patientManagementService.updatePersonalAnamnesis(id, res);
        }),
        tap(() => {
          this.notificationService.success('Záznam bol úspešne upravený.');
          this.fetchAnamnesis$.next();
        }),
        catchError((err) => {
          this.notificationService.error('Záznam sa nepodarilo upraviť.');
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }

  handlePagination() {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        page: this.paginator.pageIndex + 1,
        pageSize: this.paginator.pageSize,
      },
      queryParamsHandling: 'merge',
    });
  }
}
