import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { concatMap, map, takeUntil, tap } from 'rxjs';
import { DisposableComponent } from 'src/app/common/components/disposable/disposable.component';
import { RouteStateService } from 'src/app/utils/services/route-state.service';
import { PatientManagementService } from '../../services/patient-management.service';
import {
  PatientsDetailDto,
  RecordResponseDto,
} from '../../types/patient-management.type';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent
  extends DisposableComponent
  implements OnInit, AfterViewInit
{
  patientId: string;
  patientData: PatientsDetailDto;
  patientRecords: RecordResponseDto[];
  resultsLength = 0;
  @ViewChild('paginator') paginator: MatPaginator;

  constructor(
    private readonly routeStateService: RouteStateService,
    private readonly patientManagementService: PatientManagementService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router
  ) {
    super();
  }

  ngAfterViewInit() {
    this.activatedRoute.queryParams
      .pipe(
        takeUntil(this.destroySignal$),
        tap((qp) => {
          this.paginator.pageSize = Number(qp.pageSize) ?? 10;
          this.paginator.pageIndex = Number(qp.page) - 1 ?? 0;
        }),
        concatMap((qp) =>
          this.patientManagementService.getPatientsRecords(
            this.patientId,
            qp.page,
            qp.pageSize
          )
        ),
        map((res) => {
          this.patientRecords = res.items;
          this.resultsLength = res.meta.totalItems;
        })
      )
      .subscribe();
  }

  ngOnInit(): void {
    this.routeStateService.pathParam
      .pipe(
        takeUntil(this.destroySignal$),
        map((value) => (this.patientId = value.id)),
        concatMap(() =>
          this.patientManagementService.getPatient(this.patientId)
        ),
        map((res) => (this.patientData = res))
      )
      .subscribe();
  }

  handlePagination() {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {
        page: this.paginator.pageIndex + 1,
        pageSize: this.paginator.pageSize,
      },
      queryParamsHandling: 'merge',
    });
  }
}
