import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-anamnesis-dialog',
  templateUrl: './delete-anamnesis-dialog.component.html',
  styleUrls: ['./delete-anamnesis-dialog.component.scss'],
})
export class DeleteAnamnesisDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<DeleteAnamnesisDialogComponent>) {
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {}

  onClick(result: string): void {
    this.dialogRef.close(result);
  }
}
