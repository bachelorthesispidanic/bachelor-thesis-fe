import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { ActivatedRoute, Router } from '@angular/router';
import {
  catchError,
  concatMap,
  debounceTime,
  distinctUntilChanged,
  forkJoin,
  map,
  of,
  take,
  takeUntil,
  tap,
  throwError,
} from 'rxjs';
import { DisposableComponent } from 'src/app/common/components/disposable/disposable.component';
import { NotificationService } from 'src/app/common/services/notification.service';
import { RouteStateService } from 'src/app/utils/services/route-state.service';
import { WindowRef, WINDOW_REF } from 'src/app/utils/window-ref';
import { PatientManagementService } from '../../services/patient-management.service';
import {
  DiagnoseDto,
  MedicalNeedDto,
  MedicineDto,
  NewPrescriptionDto,
  NewRecordDto,
  ProcedureDto,
} from '../../types/patient-management.type';

@Component({
  selector: 'app-new-record',
  templateUrl: './new-record.component.html',
  styleUrls: ['./new-record.component.scss'],
})
export class NewRecordComponent extends DisposableComponent implements OnInit {
  recordForm: FormGroup;
  patientId: string;

  // suggestions
  diagnoseSuggestions: DiagnoseDto[] = [];
  procedureSuggestions: ProcedureDto[] = [];
  medicineSuggestions: MedicineDto[] = [];
  medicalNeedSuggestions: MedicalNeedDto[] = [];

  // selected options
  selectedDiagnoses: DiagnoseDto[] = [];
  selectedProcedures: ProcedureDto[] = [];
  selectedMedicines: MedicineDto[] = [];
  selectedMedicalNeeds: MedicalNeedDto[] = [];

  // refs
  @ViewChild('diagnoseInputNative', { static: false })
  diagnoseInputNative: ElementRef<HTMLInputElement>;
  @ViewChild('diagnoseSuggestionsAuto', { static: false })
  diagnoseSuggestionsAuto: MatAutocomplete;
  @ViewChild('procedureInputNative', { static: false })
  procedureInputNative: ElementRef<HTMLInputElement>;
  @ViewChild('procedureSuggestionsAuto', { static: false })
  procedureSuggestionsAuto: MatAutocomplete;
  @ViewChild('medicineInputNative', { static: false })
  medicineInputNative: ElementRef<HTMLInputElement>;
  @ViewChild('medicineSuggestionsAuto', { static: false })
  medicineSuggestionsAuto: MatAutocomplete;
  @ViewChild('medicalNeedInputNative', { static: false })
  medicalNeedInputNative: ElementRef<HTMLInputElement>;
  @ViewChild('medicalNeedSuggestionsAuto', { static: false })
  medicalNeedSuggestionsAuto: MatAutocomplete;

  constructor(
    private readonly patientManagementService: PatientManagementService,
    private readonly routeStateService: RouteStateService,
    @Inject(WINDOW_REF) private readonly window: WindowRef,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute
  ) {
    super();
    this.recordForm = new FormGroup({
      description: new FormControl('', Validators.required),
      diagnose: new FormControl(''),
      procedure: new FormControl(''),
      medicine: new FormControl(''),
      medicalNeed: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.routeStateService.pathParam
      .pipe(
        takeUntil(this.destroySignal$),
        map((value) => (this.patientId = value.id))
      )
      .subscribe();

    this.recordForm
      .get('diagnose')
      .valueChanges.pipe(
        takeUntil(this.destroySignal$),
        debounceTime(300),
        distinctUntilChanged(),
        concatMap((value) =>
          this.patientManagementService.searchDiagnoses(value)
        ),
        map((res) => (this.diagnoseSuggestions = res))
      )
      .subscribe();

    this.recordForm
      .get('procedure')
      .valueChanges.pipe(
        takeUntil(this.destroySignal$),
        debounceTime(300),
        distinctUntilChanged(),
        concatMap((value) =>
          this.patientManagementService.searchProcedures(value)
        ),
        map((res) => (this.procedureSuggestions = res))
      )
      .subscribe();

    this.recordForm
      .get('medicine')
      .valueChanges.pipe(
        takeUntil(this.destroySignal$),
        debounceTime(300),
        distinctUntilChanged(),
        concatMap((value) =>
          this.patientManagementService.searchMedicines(value)
        ),
        map((res) => (this.medicineSuggestions = res))
      )
      .subscribe();

    this.recordForm
      .get('medicalNeed')
      .valueChanges.pipe(
        takeUntil(this.destroySignal$),
        debounceTime(300),
        distinctUntilChanged(),
        concatMap((value) =>
          this.patientManagementService.searchMedicalNeeds(value)
        ),
        map((res) => (this.medicalNeedSuggestions = res))
      )
      .subscribe();
  }

  onDiagnoseSelected(e: MatAutocompleteSelectedEvent) {
    if (!this.selectedDiagnoses.find((d) => d.id === e.option.value.id)) {
      this.selectedDiagnoses.push(e.option.value);
    }
    this.diagnoseInputNative.nativeElement.value = '';
    this.recordForm.get('diagnose').setValue(null);
  }

  removeDiagnose(diagnose) {
    this.selectedDiagnoses = this.selectedDiagnoses.filter(
      (d) => d.id !== diagnose.id
    );
  }

  onProcedureSelected(e: MatAutocompleteSelectedEvent): void {
    if (!this.selectedProcedures.find((p) => p.id === e.option.value.id)) {
      this.selectedProcedures.push(e.option.value);
    }
    this.procedureInputNative.nativeElement.value = '';
    this.recordForm.get('procedure').setValue(null);
  }

  removeProcedure(procedure) {
    this.selectedProcedures = this.selectedProcedures.filter(
      (p) => p.id !== procedure.id
    );
  }

  onMedicineSelected(e: MatAutocompleteSelectedEvent): void {
    if (!this.selectedMedicines.find((m) => m.id === e.option.value.id)) {
      this.selectedMedicines.push(e.option.value);
    }
    this.medicineInputNative.nativeElement.value = '';
    this.recordForm.get('medicine').setValue(null);
  }

  removeMedicine(medicine) {
    this.selectedMedicines = this.selectedMedicines.filter(
      (m) => m.id !== medicine.id
    );
  }

  onMedicalNeedSelected(e: MatAutocompleteSelectedEvent): void {
    if (!this.selectedMedicalNeeds.find((m) => m.id === e.option.value.id)) {
      this.selectedMedicalNeeds.push(e.option.value);
    }
    this.medicalNeedInputNative.nativeElement.value = '';
    this.recordForm.get('medicalNeed').setValue(null);
  }

  removeMedicalNeed(medicalNeed) {
    this.selectedMedicalNeeds = this.selectedMedicalNeeds.filter(
      (m) => m.id !== medicalNeed.id
    );
  }

  getMedicineInfo(code: string) {
    this.patientManagementService
      .getMedicineInfo(code)
      .pipe(
        take(1),
        tap((res) => this.window.open(res.url)),
        catchError((err) => {
          this.notificationService.error('Hľadaný liek sa nenašiel');
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }

  get isFormValid() {
    return (
      this.recordForm.valid &&
      this.selectedDiagnoses.length > 0 &&
      this.selectedProcedures.length > 0
    );
  }

  onSubmit(): void {
    const record: NewRecordDto = {
      description: this.recordForm.get('description').value,
      diagnoseIds: this.selectedDiagnoses.map((d) => d.id),
      procedureIds: this.selectedProcedures.map((p) => p.id),
      patientId: this.patientId,
    };

    this.patientManagementService
      .createRecord(record)
      .pipe(
        take(1),
        concatMap((record) => {
          const medicines = this.selectedMedicines.map((m) => ({
            patientId: this.patientId,
            recordId: record.id,
            medicineId: m.id,
            medicalNeedId: null,
          }));

          console.log(medicines);

          const medicalNeeds = this.selectedMedicalNeeds.map((m) => ({
            patientId: this.patientId,
            recordId: record.id,
            medicineId: null,
            medicalNeedId: m.id,
          }));

          console.log(medicalNeeds);

          const prescriptions: NewPrescriptionDto[] =
            medicines.concat(medicalNeeds);

          if (prescriptions.length > 0) {
            const obs = prescriptions.map((p) =>
              this.patientManagementService.createPrescription(p)
            );

            return forkJoin(obs);
          } else {
            return of(true);
          }
        }),
        tap(() => {
          this.notificationService.success('Dekurz bol vytvorený');
          this.router.navigate(['../history'], {
            relativeTo: this.activatedRoute,
          });
        }),
        catchError((err) => {
          this.notificationService.error('Dekurz sa nepodarilo vytvoriť');
          return throwError(() => new Error(err));
        })
      )
      .subscribe();
  }
}
