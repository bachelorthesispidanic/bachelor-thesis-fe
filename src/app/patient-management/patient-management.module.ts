import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { NewRecordComponent } from './components/new-record/new-record.component';
import { HistoryComponent } from './components/history/history.component';
import { PharmacologicalAnamnesisComponent } from './components/pharmacological-anamnesis/pharmacological-anamnesis.component';
import { PersonalAnamnesisComponent } from './components/personal-anamnesis/personal-anamnesis.component';
import { FamilyAnamnesisComponent } from './components/family-anamnesis/family-anamnesis.component';
import { PrescriptionsComponent } from './components/prescriptions/prescriptions.component';
import { RouterModule } from '@angular/router';
import { PATIENT_MANAGEMENT_ROUTES } from './patient-management.routes';
import { MaterialModule } from '../material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AnamnesisDialogComponent } from './components/anamnesis-dialog/anamnesis-dialog.component';
import { DeleteAnamnesisDialogComponent } from './components/delete-anamnesis-dialog/delete-anamnesis-dialog.component';
import { PrescriptionDialogComponent } from './components/prescription-dialog/prescription-dialog.component';

@NgModule({
  declarations: [
    SidenavComponent,
    NewRecordComponent,
    HistoryComponent,
    PharmacologicalAnamnesisComponent,
    PersonalAnamnesisComponent,
    FamilyAnamnesisComponent,
    PrescriptionsComponent,
    AnamnesisDialogComponent,
    DeleteAnamnesisDialogComponent,
    PrescriptionDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(PATIENT_MANAGEMENT_ROUTES),
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
  ],
})
export class PatientManagementModule {}
