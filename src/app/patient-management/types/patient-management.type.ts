export interface PatientsDetailDto {
  id: string;
  titlesBeforeName: string;
  firstName: string;
  lastName: string;
  titlesAfterName: string;
  identificationNumber: string;
  birthDate: string;
  phone: string;
  email: string;
  street: string;
  houseNumber: string;
  zipCode: string;
  city: string;
  country: string;
  insuranceCode: number;
  weight: number;
  height: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: null;
  sex: SexDto;
}

export interface SexDto {
  id: string;
  code: string;
  abbreviation: string;
  name: string;
  nameEn: string;
}

export interface DiagnoseDto {
  id: string;
  code: string;
  abbreviation: string;
  name: string;
  description: string;
}

export interface ProcedureDto {
  id: string;
  code: string;
  abbreviation: string;
  name: string;
  description: string;
}

export interface MedicineDto {
  id: string;
  sukl_kod: string;
  nazov: string;
  doplnok: string;
  kod_drz: string;
  kod_statu: string;
  atc_kod: string;
  atc_nazov_sk: string;
  is: string;
  reg_cislo: string;
  expiracia: string;
  vydaj: string;
  typ_reg: string;
  datum_reg: string;
  platnost: string;
  bp: string;
}

export interface MedicalNeedDto {
  id: string;
  zp_id: string;
  zp_kod: string;
  zp_nazov: string;
  zp_nazov1: string;
  zp_doplnok: string;
  zp_doplnok1: string;
  zp_stav_kod: string;
  zp_stav_nazov: string;
  zp_platnost: string;
  kat_kod: string;
  vyr_kod: string;
  vyr_sta_kod: string;
  tr_kod: string;
  tr_nazov: string;
  zas_kod: string;
}

export interface NewRecordDto {
  description: string;
  patientId: string;
  diagnoseIds: string[];
  procedureIds: string[];
}

export interface NewPrescriptionDto {
  patientId: string;
  recordId?: string;
  medicineId?: string;
  medicalNeedId?: string;
}

export interface RecordResponseDto {
  id: string;
  description: string;
  employee: EmployeesDetailDto;
  patient: PatientsDetailDto;
  diagnoses: DiagnoseDto[];
  procedures: ProcedureDto[];
  deletedAt: string;
  createdAt: string;
  updatedAt: string;
}

export interface EmployeesDetailDto {
  id: string;
  titlesBeforeName: string;
  firstName: string;
  lastName: string;
  titlesAfterName: string;
  code: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
  medicType: MedicTypeDto;
  expertise: ExpertiseDto;
}

export interface MedicTypeDto {
  id: string;
  code: string;
  abbreviation: string;
  name: string;
}

export interface ExpertiseDto {
  id: string;
  code: string;
  abbreviation: string;
  name: string;
}

export interface PrescriptionResponseDto {
  employee: EmployeesDetailDto;
  patient: PatientsDetailDto;
  record: {
    id: string;
    description: string;
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
  };
  medicalNeed: MedicalNeedDto;
  medicine: MedicineDto;
  pickedUpAt: string;
  deletedAt: string;
  id: string;
  createdAt: string;
  updatedAt: string;
}

export interface NewAnamnesisDto {
  patientId: string;
  description: string;
}

export interface AnamnesisResponseDto extends NewAnamnesisDto {
  id: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
  patient: PatientsDetailDto;
}
