import { Route } from '@angular/router';
import { FamilyAnamnesisComponent } from './components/family-anamnesis/family-anamnesis.component';
import { HistoryComponent } from './components/history/history.component';
import { NewRecordComponent } from './components/new-record/new-record.component';
import { PersonalAnamnesisComponent } from './components/personal-anamnesis/personal-anamnesis.component';
import { PharmacologicalAnamnesisComponent } from './components/pharmacological-anamnesis/pharmacological-anamnesis.component';
import { PrescriptionsComponent } from './components/prescriptions/prescriptions.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';

export const PATIENT_MANAGEMENT_ROUTES: Route[] = [
  {
    path: ':id',
    component: SidenavComponent,
    children: [
      {
        path: '',
        redirectTo: 'history',
        pathMatch: 'full',
      },
      {
        path: 'history',
        component: HistoryComponent,
      },
      {
        path: 'new-record',
        component: NewRecordComponent,
      },
      {
        path: 'personal-anamnesis',
        component: PersonalAnamnesisComponent,
      },
      {
        path: 'family-anamnesis',
        component: FamilyAnamnesisComponent,
      },
      {
        path: 'pharmacological-anamnesis',
        component: PharmacologicalAnamnesisComponent,
      },
      {
        path: 'prescriptions',
        component: PrescriptionsComponent,
      },
    ],
  },
];
