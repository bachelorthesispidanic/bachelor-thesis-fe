import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PaginationResponseDto } from 'src/app/common/types/common.type';
import { environment } from 'src/environments/environment';
import {
  AnamnesisResponseDto,
  DiagnoseDto,
  MedicalNeedDto,
  MedicineDto,
  NewAnamnesisDto,
  NewPrescriptionDto,
  NewRecordDto,
  PatientsDetailDto,
  ProcedureDto,
  RecordResponseDto,
} from '../types/patient-management.type';

@Injectable({
  providedIn: 'root',
})
export class PatientManagementService {
  constructor(private readonly http: HttpClient) {}

  getPatient(id: string) {
    console.log(id);
    return this.http.get<PatientsDetailDto>(
      environment.baseUrl + `patient/${id}`
    );
  }

  searchDiagnoses(search: string) {
    return this.http.get<DiagnoseDto[]>(environment.baseUrl + 'diagnose', {
      params: { search: search },
    });
  }

  searchProcedures(search: string) {
    return this.http.get<ProcedureDto[]>(environment.baseUrl + 'procedure', {
      params: { search: search },
    });
  }

  searchMedicines(search: string) {
    return this.http.get<MedicineDto[]>(environment.baseUrl + 'medicine', {
      params: { search: search },
    });
  }

  searchMedicalNeeds(search: string) {
    return this.http.get<MedicalNeedDto[]>(
      environment.baseUrl + 'medical-need',
      {
        params: { search: search },
      }
    );
  }

  getMedicineInfo(search: string) {
    return this.http.get<{ url: string }>(
      environment.baseUrl + 'medicine/info-url',
      {
        params: { search: search },
      }
    );
  }

  createRecord(record: NewRecordDto) {
    return this.http.post<RecordResponseDto>(
      environment.baseUrl + 'record/new',
      record
    );
  }

  createPrescription(prescription: NewPrescriptionDto) {
    return this.http.post(
      environment.baseUrl + 'prescription/new',
      prescription
    );
  }

  getPatientsPrescriptions(id: string, page: number, pageSize: number) {
    return this.http.get<PaginationResponseDto<any[]>>(
      environment.baseUrl + `prescription/patient/${id}`,
      {
        params: {
          page: page ?? 1,
          limit: pageSize ?? 10,
        },
      }
    );
  }

  getPatientsRecords(id: string, page: number, pageSize: number) {
    return this.http.get<PaginationResponseDto<RecordResponseDto[]>>(
      environment.baseUrl + `record/patient/${id}`,
      {
        params: {
          page: page ?? 1,
          limit: pageSize ?? 10,
        },
      }
    );
  }

  createPersonalAnamnesis(anamnesis: NewAnamnesisDto) {
    return this.http.post(
      environment.baseUrl + 'personal-anamnesis',
      anamnesis
    );
  }

  getPersonalAnamnesis(patientId: string, page: number, pageSize: number) {
    return this.http.get<PaginationResponseDto<AnamnesisResponseDto[]>>(
      environment.baseUrl + `personal-anamnesis/patient/${patientId}`,
      {
        params: {
          page: page ?? 1,
          limit: pageSize ?? 10,
        },
      }
    );
  }

  deletePersonalAnamnesis(id: string) {
    return this.http.delete(environment.baseUrl + `personal-anamnesis/${id}`);
  }

  updatePersonalAnamnesis(id: string, description: string) {
    return this.http.patch(environment.baseUrl + `personal-anamnesis/${id}`, {
      description: description,
    });
  }

  createFamilyAnamnesis(anamnesis: NewAnamnesisDto) {
    return this.http.post(environment.baseUrl + 'family-anamnesis', anamnesis);
  }

  getFamilyAnamnesis(patientId: string, page: number, pageSize: number) {
    return this.http.get<PaginationResponseDto<AnamnesisResponseDto[]>>(
      environment.baseUrl + `family-anamnesis/patient/${patientId}`,
      {
        params: {
          page: page ?? 1,
          limit: pageSize ?? 10,
        },
      }
    );
  }

  deleteFamilyAnamnesis(id: string) {
    return this.http.delete(environment.baseUrl + `family-anamnesis/${id}`);
  }

  updateFamilyAnamnesis(id: string, description: string) {
    return this.http.patch(environment.baseUrl + `family-anamnesis/${id}`, {
      description: description,
    });
  }

  createPharmacologicalAnamnesis(anamnesis: NewAnamnesisDto) {
    return this.http.post(
      environment.baseUrl + 'pharmacological-anamnesis',
      anamnesis
    );
  }

  getPharmacologicalAnamnesis(
    patientId: string,
    page: number,
    pageSize: number
  ) {
    return this.http.get<PaginationResponseDto<AnamnesisResponseDto[]>>(
      environment.baseUrl + `pharmacological-anamnesis/patient/${patientId}`,
      {
        params: {
          page: page ?? 1,
          limit: pageSize ?? 10,
        },
      }
    );
  }

  deletePharmacologicalAnamnesis(id: string) {
    return this.http.delete(
      environment.baseUrl + `pharmacological-anamnesis/${id}`
    );
  }

  updatePharmacologicalAnamnesis(id: string, description: string) {
    return this.http.patch(
      environment.baseUrl + `pharmacological-anamnesis/${id}`,
      { description: description }
    );
  }
}
