import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { AuthModule } from './auth/auth.module';
import { CommonAppModule } from './common/common.module';
import { NgxsModule } from '@ngxs/store';
import { AppState } from './state/app.state';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { environment } from 'src/environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './common/interceptors/http-error.interceptor';
import { HttpRequestInterceptor } from './common/interceptors/http-request.interceptor';
import { HttpResponseInterceptor } from './common/interceptors/http-response.interceptor';

const NGXS_DEPS = [
  NgxsStoragePluginModule.forRoot({
    key: ['auth', 'user'],
  }),
  NgxsModule.forRoot([], {
    developmentMode: !environment.production,
    selectorOptions: {
      // upcoming v4 compatibility
      injectContainerState: false,
    },
  }),
  NgxsFormPluginModule.forRoot(),
  NgxsReduxDevtoolsPluginModule.forRoot({
    disabled: environment.production,
    name: 'NGXS_AUCLAND',
  }),
];
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    NGXS_DEPS,
    AuthModule,
    CommonAppModule,
    NgxsModule.forFeature([AppState]),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpResponseInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
