import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';

@Injectable()
export class PaginatorUtil extends MatPaginatorIntl {
    constructor() {
        super();

        this.getAndInitTranslations();
    }

    getAndInitTranslations() {
        this.itemsPerPageLabel = 'Počet položiek na stránku:';
        this.nextPageLabel = 'Následujúca strana';
        this.previousPageLabel = 'Predchádzajúca strana';
        this.changes.next();
    }

    getRangeLabel = (page: number, pageSize: number, length: number) => {
        if (length === 0 || pageSize === 0) {
            return `Strana 0 z ${length}`;
        }
        return `Strana ${page + 1} z ${Math.ceil(length / pageSize)}`;
    };
}
