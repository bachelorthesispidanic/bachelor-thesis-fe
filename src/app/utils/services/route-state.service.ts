import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  ReplaySubject,
  distinctUntilChanged,
  skip,
  first,
  take,
} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RouteStateService {
  private pathParamState = new BehaviorSubject<{ id: string }>({
    id: '',
  });
  pathParam: Observable<{ id: string }>;
  initializedSignal$ = new ReplaySubject<{ id: string }>();

  get pathParamValue() {
    return this.pathParamState.value;
  }

  get pathId() {
    return this.pathParamValue.id;
  }

  constructor() {
    this.pathParam = this.pathParamState
      .asObservable()
      .pipe(distinctUntilChanged());
    this.pathParam
      .pipe(skip(1), first(), take(1))
      .subscribe(() => this.initializedSignal$.next(this.pathParamValue));
    console.log('[Route state constructor]', this.pathParamValue);
  }

  updatePathParamState(newPathParam: Partial<{ id: string }>) {
    const mergeParam = { ...this.pathParamValue, ...newPathParam };
    this.pathParamState.next(mergeParam);
    console.log('[Route state update fn]', this.pathParamValue);
  }
}
