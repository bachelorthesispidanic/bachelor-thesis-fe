import { AbstractControl } from '@angular/forms';

export const emailValidator = (control: AbstractControl) => {
  const email: string = control.value || '';

  if (!email) {
    return null;
  }

  // tslint:disable-next-line: max-line-length
  const emailFormat =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!emailFormat.test(email)) {
    return {
      incorrectEmail: 'Not a valid email address',
    };
  }
  return null;
};
