import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';

export const equalityValidator = (
  matchTo: string,
  errorOnNotEqual: boolean = true
) => {
  return (control: AbstractControl): ValidationErrors | null => {
    return !!control.parent &&
      !!control.parent.value &&
      control.value ===
        ((control.parent as FormGroup).controls[matchTo] as AbstractControl)
          .value
      ? errorOnNotEqual
        ? null
        : { notEqual: true }
      : errorOnNotEqual
      ? { notEqual: true }
      : null;
  };
};
