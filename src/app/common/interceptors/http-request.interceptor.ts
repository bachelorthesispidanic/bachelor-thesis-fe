import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpContextToken,
  HttpErrorResponse,
} from '@angular/common/http';
import {
  BehaviorSubject,
  catchError,
  filter,
  Observable,
  switchMap,
  take,
  takeUntil,
  throwError,
} from 'rxjs';
import { LoadingService } from '../services/loading.service';
import { Store } from '@ngxs/store';
import { AuthState } from 'src/app/auth/state/auth.state';
import { environment } from 'src/environments/environment';
import { UpdateAuthState } from 'src/app/auth/state/auth.actions';
import { HttpCancelService } from '../services/http-cancel.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import { RefreshTokenResponseDto } from 'src/app/auth/types/auth.type';

export const BYPASS_LOADING = new HttpContextToken(() => false);
export const BYPASS_INTERCEPTOR = new HttpContextToken(() => false);

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );

  constructor(
    private readonly loadingService: LoadingService,
    private readonly authService: AuthService,
    private readonly httpCancelService: HttpCancelService,
    private readonly store: Store
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const isApiRequest = request.urlWithParams.startsWith(environment.baseUrl);

    if (request.context.get(BYPASS_LOADING) === false) {
      this.loadingService.showLoading();
    }

    if (request.context.get(BYPASS_INTERCEPTOR) === true || !isApiRequest) {
      return next.handle(request);
    } else {
      const clonedRequest = this.addAccessToken(request);

      return next.handle(clonedRequest).pipe(
        catchError((error) => {
          if (error instanceof HttpErrorResponse && error.status === 401) {
            return this.handle401Error(clonedRequest, next);
          } else {
            return throwError(error);
          }
        })
      );
    }
  }

  private addAccessToken(request: HttpRequest<unknown>) {
    const accessToken = this.store.selectSnapshot(AuthState.accessToken);
    return request.clone({
      headers: accessToken
        ? request.headers.set('Authorization', `Bearer ${accessToken}`)
        : request.headers,
    });
  }

  private handle401Error(request: HttpRequest<unknown>, next: HttpHandler) {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);
      const refreshToken = this.store.selectSnapshot(AuthState.refreshToken);

      return this.authService.performTokenRefresh(refreshToken).pipe(
        switchMap((data: RefreshTokenResponseDto) => {
          this.store.dispatch(new UpdateAuthState(data));
          this.isRefreshing = false;
          this.refreshTokenSubject.next(data.accessToken);
          return next.handle(this.addAccessToken(request));
        }),
        catchError((error) => {
          this.isRefreshing = false;
          this.refreshTokenSubject.next(null);
          this.httpCancelService.cancelPendingRequests();
          this.authService.performLogout();
          return throwError(error);
        })
      );
    } else {
      return this.refreshTokenSubject.pipe(
        takeUntil(this.httpCancelService.onCancelPendingRequests()),
        filter((token) => token != null),
        take(1),
        switchMap(() => {
          return next.handle(this.addAccessToken(request));
        })
      );
    }
  }
}
