import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LoadingService } from '../services/loading.service';
import { NotificationService } from '../services/notification.service';
import { getReasonPhrase } from 'http-status-codes';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(
    private readonly notificationService: NotificationService,
    private readonly loadingService: LoadingService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        this.loadingService.hideLoading();
        if (error.status >= 500 || error.status == 0) {
          const errMessage =
            error.status === 0
              ? `Portal connection lost, trying to reconnect`
              : error.error.message || getReasonPhrase(error.status);

          this.notificationService.error(`Error: ${errMessage}`);
        }
        return throwError(error);
      })
    );
  }
}
