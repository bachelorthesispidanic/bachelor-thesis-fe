import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { LoadingComponent } from './components/loading/loading.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MobileNavbarComponent } from './components/mobile-navbar/mobile-navbar.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { MaterialModule } from '../material.module';
import { ErrorNotificationComponent } from './components/notifications/error-notification/error-notification.component';
import { InfoNotificationComponent } from './components/notifications/info-notification/info-notification.component';
import { SuccessNotificationComponent } from './components/notifications/success-notification/success-notification.component';
import { WarningNotificationComponent } from './components/notifications/warning-notification/warning-notification.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    HeaderComponent,
    LoadingComponent,
    NavbarComponent,
    MobileNavbarComponent,
    NotFoundComponent,
    WarningNotificationComponent,
    SuccessNotificationComponent,
    ErrorNotificationComponent,
    InfoNotificationComponent,
  ],
  imports: [CommonModule, MaterialModule, RouterModule],
  exports: [
    HeaderComponent,
    LoadingComponent,
    NavbarComponent,
    MobileNavbarComponent,
    NotFoundComponent,
    WarningNotificationComponent,
    SuccessNotificationComponent,
    ErrorNotificationComponent,
    InfoNotificationComponent,
  ],
})
export class CommonAppModule {}
