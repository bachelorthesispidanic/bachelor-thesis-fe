import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { WindowRef, WINDOW_REF } from 'src/app/utils/window-ref';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  screenWidth: number;

  constructor(@Inject(WINDOW_REF) private readonly window: WindowRef) {}

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = this.window.innerWidth;
  }

  ngOnInit(): void {
    this.screenWidth = this.window.innerWidth;
  }
}
