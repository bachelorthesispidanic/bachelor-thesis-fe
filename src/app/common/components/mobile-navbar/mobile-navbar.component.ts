import { trigger, transition, style, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { takeUntil } from 'rxjs';
import { UpdateAppState } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';
import { DisposableComponent } from '../disposable/disposable.component';

@Component({
  selector: 'app-mobile-navbar',
  templateUrl: './mobile-navbar.component.html',
  styleUrls: ['./mobile-navbar.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('0.6s ease-out', style({ height: 'fit-content', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ height: 'fit-content', opacity: 1 }),
        animate('0.6s ease-in', style({ height: 0, opacity: 0 })),
      ]),
    ]),
  ],
})
export class MobileNavbarComponent
  extends DisposableComponent
  implements OnInit
{
  currentSidenavState: boolean;

  constructor(private readonly store: Store, private readonly router: Router) {
    super();
  }

  ngOnInit(): void {
    this.store
      .select(AppState.isSidenavOpened)
      .pipe(takeUntil(this.destroySignal$))
      .subscribe((data: boolean) => (this.currentSidenavState = data));
  }

  toggleSidenav() {
    this.store.dispatch(
      new UpdateAppState({ isSidenavOpened: !this.currentSidenavState })
    );
  }
}
