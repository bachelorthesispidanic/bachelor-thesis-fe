import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, StateToken } from '@ngxs/store';
import { UpdateAppState } from './app.actions';
import type { AppStateModel } from './app.model';

const APP_STATE_TOKEN = new StateToken<AppStateModel>('app');

function createInitialState(): AppStateModel {
  const init = window.innerWidth > 959 ? true : false;
  return {
    isSidenavOpened: init,
  };
}

@State({
  name: APP_STATE_TOKEN,
  defaults: createInitialState(),
})
@Injectable()
export class AppState {
  @Selector([AppState])
  static isSidenavOpened(state: AppStateModel) {
    return state.isSidenavOpened;
  }

  @Action(UpdateAppState)
  public patch(
    { patchState }: StateContext<AppStateModel>,
    { payload }: UpdateAppState
  ) {
    return patchState(payload);
  }
}
