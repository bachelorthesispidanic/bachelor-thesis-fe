import type { AppStateModel } from './app.model';

class SetAppState {
    static readonly type = '[App] Set app state';
    constructor(public payload: AppStateModel) {}
}

class UpdateAppState {
    static readonly type = '[App] Update app state';
    constructor(public payload: Partial<AppStateModel>) {}
}

export { SetAppState, UpdateAppState };
