import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { MatDialog } from '@angular/material/dialog';
import { MatDrawerMode } from '@angular/material/sidenav';
import { Router, NavigationEnd } from '@angular/router';
import { Actions, Store, ofActionDispatched } from '@ngxs/store';
import { Observable, takeUntil } from 'rxjs';
import { Logout } from './auth/state/auth.actions';
import { DisposableComponent } from './common/components/disposable/disposable.component';
import { NotificationService } from './common/services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent
  extends DisposableComponent
  implements OnInit, AfterViewInit
{
  title = 'PFmed';
  sidenavOpened$: Observable<boolean> | undefined;
  sidenavMode$: Observable<MatDrawerMode>;
  @ViewChild('appContent') appContent: ElementRef;

  constructor(
    private readonly router: Router,
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly mediaObserver: MediaObserver,
    private readonly notificationService: NotificationService,
    public dialog: MatDialog
  ) {
    super();
  }

  ngOnInit() {
    this.actions$
      .pipe(ofActionDispatched(Logout), takeUntil(this.destroySignal$))
      .subscribe(() => this.router.navigate(['']));
  }

  ngAfterViewInit() {
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        if (
          document.getElementsByClassName('mat-drawer-inner-container').length
        ) {
          document
            .getElementsByClassName('mat-drawer-inner-container')[0]
            .scrollTo(0, 0);
        }

        this.appContent.nativeElement.scrollTo(0, 0);
      }
    });
  }
}
