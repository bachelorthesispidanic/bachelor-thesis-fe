export const environment = {
  production: false,
  baseUrl: 'http://localhost:8000/',
  assetsUrl: 'http://localhost:4200/assets',
};
