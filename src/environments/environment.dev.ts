export const environment = {
  production: false,
  baseUrl: 'https://api.lekar.pfmed.sk/',
  assetsUrl: 'https://lekar.pfmed.sk/assets',
};
