export const environment = {
  production: true,
  baseUrl: 'https://api.lekar.pfmed.sk/',
  assetsUrl: 'https://lekar.pfmed.sk/assets',
};
